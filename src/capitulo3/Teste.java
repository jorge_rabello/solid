package capitulo3;

public class Teste {

    public static void main(String[] args) {

        // note que aqui podemos utilizar qualquer tipo de tabela de preco
        TabelaDePreco tabela = new TabelaDePrecoPadrao();

        // note que aqui podemos utilizar qualquer servico de entrega
        ServicoDeEntrega entrega = new Frete();
        new CalculadoraDePrecos(tabela, entrega);

        // Por exemplo
        // veja que trocamos a implementacao mas a calculadora de precos continua funcionando
        TabelaDePreco tabelaDiferenciada = new TabelaDePrecoDiferenciada();
        ServicoDeEntrega entregaCorreios = new Correios();
        new CalculadoraDePrecos(tabelaDiferenciada, entrega);
    }
}
