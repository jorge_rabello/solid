package capitulo5;

public class ContaComum {

    private ManipuladorDeSaldo manipuladorDeSaldo;

    public ContaComum(ManipuladorDeSaldo manipuladorDeSaldo) {
        this.manipuladorDeSaldo = manipuladorDeSaldo;
    }

    public void saca(double valor) {
        manipuladorDeSaldo.saca(valor);
    }

    public void deposita(double valor) {
        manipuladorDeSaldo.deposita(valor);
    }

    public void rende() {
        this.manipuladorDeSaldo.rende(0.01);
    }

    public double getSaldo() {
        return manipuladorDeSaldo.getSaldo();
    }

}
