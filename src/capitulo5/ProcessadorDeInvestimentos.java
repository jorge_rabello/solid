package capitulo5;

import java.util.Arrays;
import java.util.List;

public class ProcessadorDeInvestimentos {

    public static void main(String[] args) {

        for (Object conta : contasDoBanco()) {
            ContaComum c = (ContaComum) conta;
            c.rende();

            System.out.println("Novo Saldo:");
            System.out.println(c.getSaldo());
        }
    }

    private static List<Object> contasDoBanco() {
        return Arrays.asList(umaContaCom(100), umaContaCom(150), contaDeEstudanteCom(200));
    }

    private static ContaDeEstudante contaDeEstudanteCom(double amount) {
        ManipuladorDeSaldo manipuladorDeSaldo = new ManipuladorDeSaldo();
        ContaDeEstudante c = new ContaDeEstudante(manipuladorDeSaldo);
        c.deposita(amount);
        return c;
    }

    private static ContaComum umaContaCom(double valor) {
        ManipuladorDeSaldo manipuladorDeSaldo = new ManipuladorDeSaldo();
        ContaComum c = new ContaComum(manipuladorDeSaldo);
        c.deposita(valor);
        return c;
    }
}
