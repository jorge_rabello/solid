package capitulo5;

public class ContaDeEstudante {

    private int milhas;
    private ManipuladorDeSaldo manipuladorDeSaldo;

    public ContaDeEstudante(ManipuladorDeSaldo manipuladorDeSaldo) {
        this.manipuladorDeSaldo = manipuladorDeSaldo;
    }

    public void deposita(double valor) {
        manipuladorDeSaldo.deposita(valor);
        this.milhas += (int) valor;
    }

    public int getMilhas() {
        return milhas;
    }

}
