package capitulo4;

public class Pagamento {

    private final double valor;
    private final MeioDePagamento boleto;

    public Pagamento(double valor, MeioDePagamento boleto) {
        this.valor = valor;
        this.boleto = boleto;
    }

    public double getValor() {
        return valor;
    }
}
